<?php
error_reporting(E_ERROR | E_PARSE );

$weather = "";

$error = "";

if (isset($_GET['city'])){
  
  $urlCont = file_get_contents('https://api.openweathermap.org/data/2.5/weather?q='.$_GET['city'].'&units=metric&appid=1cd6909eb863d4880fb91dc133f534c7');

$forcastArray = json_decode($urlCont, true);
//print_r($forcastArray);

if ($forcastArray['cod']==200) {
  
    $weather ='The weather in '.$_GET['city']. ' is '.$forcastArray['weather'][0]['description'];
    $weather = $weather.'. The temperature is '.$forcastArray['main']['temp'].'&#8451;'.'. The speed of wind is '.$forcastArray['wind']['speed'].'m/sec';

} else{
  $error = 'The city name is correct, please';

}



}
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style/style.css">

    <title>WEATHER APP</title>
  </head>
  <body>
    

  <div class="container" id="mainDiv">
      <h1>WEATHER IN YOUR CITY</h1>
  

<form>
  <div class="form-group">
    <label for="city">Input a city name</label>
    <input  class="form-control" id="city" name="city" aria-describedby="Forcast city" placeholder="Enter city name">
   
  </div>
  
 
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

  <div>
    
    <div class="alert alert-primary" role="alert">
        
        <?php

          if ($weather) {
            echo  '<div class="alert alert-primary" role="alert">'.$weather.'</div>';
          }else if ($error){
            echo  '<div class="alert alert-danger" role="alert">'.$error.'</div>';
          }
        

        ?>
    </div>

  </div>

    

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
    </div>
  </body>
</html>